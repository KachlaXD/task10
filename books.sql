-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 04, 2024 at 12:19 PM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `books`
--

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `id` int(10) UNSIGNED NOT NULL,
  `brand` varchar(20) NOT NULL,
  `door_amount` varchar(5) NOT NULL,
  `hp` varchar(5) NOT NULL,
  `torque` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`id`, `brand`, `door_amount`, `hp`, `torque`) VALUES
(1, 'BMW M5 F90', '4', '617', '750'),
(2, 'MB CLS63', '4', '518', '700'),
(3, 'AUDI RS6', '4', '621', '625'),
(4, 'Corvette C7', '2', '460', '630'),
(5, 'BMW e92 335i', '2', '306', '400');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `city_name` varchar(20) NOT NULL,
  `population` varchar(15) NOT NULL,
  `budget` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `city_name`, `population`, `budget`) VALUES
(1, 'Tbilisi', '1200000', '3000000000'),
(2, 'Kutaisi', '300000', '700000000'),
(3, 'Batumi', '600000', '1500000000'),
(4, 'Gori', '70000', '130000000'),
(5, 'Zugdidi', '70000', '1000000');

-- --------------------------------------------------------

--
-- Table structure for table `comps`
--

CREATE TABLE `comps` (
  `id` int(10) UNSIGNED NOT NULL,
  `cpu` varchar(15) NOT NULL,
  `gpu` varchar(15) NOT NULL,
  `ram` varchar(15) NOT NULL,
  `motherboard` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `comps`
--

INSERT INTO `comps` (`id`, `cpu`, `gpu`, `ram`, `motherboard`) VALUES
(1, 'i5 7400', 'rx580', '8gb', 'asus z170'),
(2, 'i7 6700k', 'gtx 1080ti', '16gb', 'asus z370'),
(3, 'i9 12700F', 'rtx 3080ti', '32gb', 'msi bag660'),
(4, 'i3 8100f', 'gtx 1050mini', '12gb', 'h310a'),
(5, 'ryzen 5 2900', 'none', '64gb', 'Prime B450M-A ');

-- --------------------------------------------------------

--
-- Table structure for table `unis`
--

CREATE TABLE `unis` (
  `id` int(10) UNSIGNED NOT NULL,
  `uni_name` varchar(25) NOT NULL,
  `students_amount` varchar(5) NOT NULL,
  `faculty_amount` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `unis`
--

INSERT INTO `unis` (`id`, `uni_name`, `students_amount`, `faculty_amount`) VALUES
(1, 'EU Uni', '6677', '15'),
(2, 'BTU', '7711', '3'),
(3, 'STU', '17000', '55'),
(4, 'TSU', '21000', '70'),
(5, 'SEU', '2456', '17');

-- --------------------------------------------------------

--
-- Table structure for table `wignebi`
--

CREATE TABLE `wignebi` (
  `id` int(11) UNSIGNED NOT NULL,
  `book_name` varchar(20) NOT NULL,
  `page_amount` varchar(4) NOT NULL,
  `article_amount` varchar(3) NOT NULL,
  `cover_color` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `wignebi`
--

INSERT INTO `wignebi` (`id`, `book_name`, `page_amount`, `article_amount`, `cover_color`) VALUES
(1, 'გზა', '691', '63', 'ლურჯი'),
(2, 'საპიენსი', '552', '32', 'წითელი'),
(3, 'თანხმობის წარმოება', '770', '56', 'კრემისფერი'),
(4, 'ჰომო დეუსი', '522', '28', 'შავი'),
(5, 'ულისე', '866', '71', 'ნაცრისფერი');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comps`
--
ALTER TABLE `comps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unis`
--
ALTER TABLE `unis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wignebi`
--
ALTER TABLE `wignebi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `comps`
--
ALTER TABLE `comps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `unis`
--
ALTER TABLE `unis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `wignebi`
--
ALTER TABLE `wignebi`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
